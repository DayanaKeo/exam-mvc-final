<?php
namespace App\Service;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }

    /**
     * textValid

     * @param chaine $chaine int
     * @param empty $empty bool
     * @return string $error
     */
    public function isNumber($chaine, $min = 0, $max = 999)
    {
        $error = '';
        if (!ctype_digit($chaine)) {
            $error = "La chaîne '{$chaine}' ne représente pas un nombre.";
        } elseif (intval($chaine) < $min || intval($chaine) > $max) {
            $error = "La chaîne '{$chaine}' doit être un nombre compris entre {$min} et {$max}.";
        }
        return $error;
    }

}
