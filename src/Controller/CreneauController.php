<?php
namespace App\Controller;

use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use App\Model\CreneauModel;
use App\Model\SalleModel;

class CreneauController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }
    public function creneau()
    {
        $salle = SalleModel::all();
        $errors = array();
        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);
            //Validation
            $errors = $this->validate($this->v,$post);

            if($this->v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Votre créneau a été validé avec succes !');
                // redirection
                $this->redirect('home');
            }
        }
        $form = new Form($errors);
        $horaires = array();
        for ($heure = 0; $heure <= 23; $heure++) {
            for ($minute = 0; $minute <= 45; $minute += 15) {
                $horaires[sprintf('%02d:%02d', $heure, $minute)] = sprintf('%02d:%02d', $heure, $minute);
            }
        }

        $this->render('app.gestion.add_creneau', array(
            'form' => $form,
            'horaires' => $horaires,
            'salle' => $salle,
        ));
    }
    private function validate($v,$post)
    {
        $errors = [];
        $errors['start_at'] = $v->textValid($post['start_at'], 'start_at',2, 150);
        $errors['nbrehours'] = $v->textValid($post['nbrehours'], 'nbrehours',1, 24);

        return $errors;
    }
}