<?php
namespace App\Controller;

use App\Model\SalleModel;

use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class SalleController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }
    public function salle()
    {
        $errors = array();
        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);
            //Validation
            $errors = $this->validate($this->v,$post);

            if($this->v->isValid($errors)) {
                SalleModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Votre salle a été ajouté avec succes !');
                // redirection
                $this->redirect('home');
            }
        }

        $form = new Form($errors);
        $this->render('app.gestion.add_salle', array(
            'form'=>$form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',5, 50);
        $errors['maxuser'] = $v->isNumber($post['maxuser']);

        return $errors;
    }
}