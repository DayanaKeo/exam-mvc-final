<?php

namespace App\Controller;

use App\Model\UserModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {

        $message = 'Bienvenue sur le framework MVC';
        $users = UserModel::all();
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'message' => $message,
            'user'=>$users,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
