<?php
namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class GestionController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }
    public function add()
    {
        $errors = array();

        if(!empty($_POST['submitted'])){
            //Faille xss
            $post = $this->cleanXss($_POST);
            //Validation
            $errors = $this->validate($this->v,$post);

            if($this->v->isValid($errors)) {
                UserModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Votre utilisateur a été enregistré avec succes !');
                // redirection
                $this->redirect('home');
            }
        }

        $form = new Form($errors);
        $this->render('app.gestion.add_user', array(
            'form'=>$form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 150);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 150);

        return $errors;
    }



}