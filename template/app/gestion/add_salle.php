<h1>Ajouter une salle</h1>

<form action="" method="post" class="wrapform" novalidate>
    <?php echo $form->label('Title :'); ?>
    <?php echo $form->input('title', 'title'); ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('Max utilisateur : ', 'maxuser'); ?>
    <?php echo $form->input('maxuser', 'number'); ?>
    <?php echo $form->error('maxuser'); ?>

    <?php echo $form->submit('submitted', 'Ajouter'); ?>
</form>