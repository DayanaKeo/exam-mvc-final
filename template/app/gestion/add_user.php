<h1>Ajouter un utilisateur</h1>

<form action="" method="post" class="wrapform" novalidate>
    <?php echo $form->label('nom :'); ?>
    <?php echo $form->input('nom', 'nom'); ?>
    <?php echo $form->error('nom'); ?>

    <?php echo $form->label('Email : '); ?>
    <?php echo $form->input('email', 'email'); ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->submit('submitted', 'Ajouter'); ?>
</form>