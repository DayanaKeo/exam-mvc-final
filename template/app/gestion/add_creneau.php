<h1>Ajouter un créneau</h1>

<form action="" method="post" class="wrapform" novalidate>
    <?php echo $form->label('salle'); ?>
    <?php echo $form->input('id_salle', $salle); ?>
    <?php echo $form->error('id_salle'); ?>

    <?php echo $form->label('Créneau'); ?>
    <?php echo $form->select('creneau', $horaires); ?>
    <?php echo $form->error('horaire'); ?>

    <?php echo $form->label('temps'); ?>
    <?php echo $form->select('temps', $horaires); ?>
    <?php echo $form->error('temps'); ?>

    <?php echo $form->submit('submitted', 'Ajouter'); ?>
</form>